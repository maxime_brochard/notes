# Préparation éval' finale

## Préparer l'environnement 
*approche bottom-up => on part du back-end puis on fait le front.*

### backend
1. créer deux dossiers dans le dossier de l'éval => backend et frontend. 
2.  Un .git dans à la racine (dossier eval)  
3. start.spring.io
	* java 13, maven, jar (pas trop de dépendances...)
	* Spring Boot 2.2.5 
4. faire un mvn package pour tester que le projet vide compile + initial commit/push

### frontend
1. Installer Angular : `npm install -g @angular/cli`
2. Générer un projet Angular : `ng new <nomduprojet>`
3. tester le lancement le projet Angular : `ng serve`
4. Générer des composants : `ng g c <nomducomposant>`

## Architecture basique du back
1. Architecture 3 tiers à implémenter : **controller**, **service**, **domain**
	- **controller** => @RestController, @GetMapping, @PostMapping, ect.
	- **service** => couche intermédiare entre le controller (côté client), et le **domaine** (communication avec bdd, règles métier, etc.).
	- Ne pas oublier d'ajouter le CORS de la bonne manière ! 

```java
@Configuration
public class WebConfiguration implementsWebMvcConfigurer{
	@Overridepublic 
	void addCorsMapping(CorsRegistry registry){
	registry.addMaping("/**").allowedMethods(*);
	}
}
```
 

## Architecture basique du front
1. Créer la couche de *service* (.ts) dans un dossier **service** (ex: ./src/app/monAppliService.ts), + le mettre dans les *providers* ainsi que HttpClient.
2.   Créer les composants nécessaires a l'affichage de la première feature implémentée dans le **backend**

## Implémentation base de données 
1. Faire un script **.sql** de création des tables, à déposer dans les *resources* du **backend**.
2. Ajouter postgres dans la liste des dépendances.
3. Ajouter les infos de connection dans application.yml.
4. Lancer un conteneur Docker de **postgres**.
5. Créer l'interface *repo* qui extends *CrudRepository* (néccesite la dépendance JPA dans le pom.xml).
6. *getter* et *setter* de la ressource à manipuler (@Entity sur la classe pour *spring*).
7. coder le *CRUD* dans les 3 couches, de manière transversale :
	* **controller + service + repo** pour chacune des fonctionnalités du *CRUD*. 
## Dockerfile du backend
* `FROM openjdk:13.0-slim`
* `COPY target/*.jar /app.jar`
	* `COPY`copie exécutable produit par maven dans `target/` vers une image située à la racine du conteneur `/` dans l’exécutable `app.jar`.
* `EXPOSE 8080` définit le port exposé aux clients à l'extérieur du conteneur. 
* `CMD java -jar /app.jar` lance exécutable.
* Ne pas oublier de commit/push quand on à une image stable de docker. 
## Dockerfile du frontend
* `FROM node:13.8.0-stretch`
* `WORKDIR /app`
* `COPY . .`
* `EXPOSE 4200` définit le port exposé aux clients à l'extérieur du conteneur. 
* `RUN npm install -g @angular/cli` installe Angular
* `CMD ng serve` lance le serveur Angular.

## docker-compose
* **Ne pas oublier** de builder le projet avant de lancer un `docker-compose up` si jamais les Dockerfile ne contiennent pas le stage *build* :
`docker build -t <nomdelimageabuilder> .`

* **Ne pas oublier** que le *application.yml* du **backend** pour démarrer la base *postgres* sur localhost, pour fonctionner via Docker,  il faut créer un fichier `application-<nomduprofil>.yml` et y placer la ligne qui démarre la base sur le profil actif: 
`url: jdbc:postgresql://<nomduservice>:5432/postgres`  

Le fichier *docker-compose.yml* se situe à la racine du projet, au meme niveau que les dossiers de *backend* et *frontend*

> docker-compose.yml
```yml
version: '3.7'
services:

 frontend:
   image: ${FRONTEND_IMAGE:-zacademy-frontend}
   container_name: zacademy-frontend
   ports:
     - 4200:4200
 
  backend:
    image: ${BACKEND_IMAGE:-zacademy-backend}
    container_name: zacademy-backend
    environment:
      SPRING_PROFILES_ACTIVE: docker 
    ports:
      - 8080:8080
  
  db:
    image: postgres:latest
    container_name: zacademy_db
    environment:
      POSTGRES_PASSWORD: myscretpassword
    volumes:
      - ./backend/src/main/resources/sql/:/docker-entrypoint-initdb.d/
      
  pgadmin: 
    image: dpage/pgadmin4:4
    container_name: zacademy-pgadmin
    environment: 
      PGADMIN_DEFAULT_EMAIL: postgres
      PGADMIN_DEFAULT_PASSWORD: admin
    ports:
      - 5050:80  
```
Les commandes magiques pour lancer les éléments du projet :

* Dans le dossier *backend*, compiler le projet *java/maven* : 
`mvn clean package`
 
* Build d'une image Docker (pour *front* et *back*, par exemple) : 
`docker build -t <nomdelimage> <chemin>`

 * Lancer les conteneurs (en deamon avec *-d*)
`docker-compose up -d`

## Pipeline d'intégration continue (Gitlab CI)
En 3 phases : *test, build, deploy* 
1. Idéalement, créer une nouvelle branche en local 
`git branch gitlab-ci && git git switch gitlab-ci` 
2. Créer un fichier *.gitlab-ci.yml* à la racine du projet
```yml
stages:
  - build
  - package
  - deploy

#gitlab env. variables
variables: 
  DOCKER_TLS_CERTDIR: "*/certs"
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"

#to make docker in docker in gitlab
services:
  - docker:19.03.1-dind

generate_jar:
  stage: build
  image: maven:3.6.3-jdk-13 #maven version of the java project
  script:
    - cd backend #enter project directory
    - mvn package #build .jar with package stage of maven lifecycle
  artifacts:
    - target/*.jar
  cache: #to accelerate further pipelines 
    paths:
      - key: maven-repo
      - .m2/repository

build_backend_image:
  stage: package
  image: docker:19.03.1
  variables: 
    BACKEND_IMAGE: $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_SHA
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $BACKEND_IMAGE backend
    - docker push $BACKEND_IMAGE
    - 
build_frontend_image:
  stage: package
  image: docker:19.03.1
  variables: 
    FRONTEND_IMAGE: $CI_REGISTRY_IMAGE/frontend:$CI_COMMIT_SHA
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $FRONTEND_IMAGE frontend
    - docker push $FRONTEND_IMAGE

deploy:
 stage: deploy
 image: docker:19.03.1
 script:
 - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
 
 # test the project with curl and a GET http request on the backend after a docker compose command
 - apk add curl
 - apk add docker-compose
 - docker-compose up -d
 - timeout 60s sh -c 'until curl http://docker:<exposedport>/path/to/resource; do sleep 2; done'
   

```

