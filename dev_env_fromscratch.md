# Prepare Linux deploy environment from scratch 
This doc summarize a Linux-based environment prepared to host a web-app. It also initialize a basic continuous integration/development (CI/CD) flow. 

***


### Summary
* **Java**
	* **Maven** as dependencies manager 
* **NodeJS**
* **Docker** (to deploy project/app)
	* **Docker Compose** (running multi-container Docker applications) 
***

### 0. First of all 
*The following process only requires a clean, prior installation of a Linux distribution (Ubuntu, Debian, etc.), without any of the prerequisites being installed on it. The process described is made on a **Raspberry Pi 3b+**, which is an **ARM** architecture. Some of the dependencies/libraries/packages versions are specific to this architecture, **but are also available for x86 and other processors**.* 

* Update/upgrade the host system
 
	`sudo apt update && sudo apt upgrade`
	
###  1. **Java**
* Download the Java Development Kit (JDK), ~ 1GB. 

	`sudo apt-install default-jdk`

* Open file **/etc/environment**
	
	`sudo nano /etc/environment` 

	* Add the following line to set the path of the **JAVA_HOME** environment variable (change java version *java-11-openjdk-armhf* if needed).

		`JAVA_HOME="/usr/lib/jvm/java-11-openjdk-armhf"`

* Verify that the **JAVA_HOME** environment variable is correctly set

	`source /etc/environment && echo $JAVA_HOME`

	* Should output : 

		`/usr/lib/jvm/java-11-openjdk-armhf`
		
* Check Java version
	
	`java -version`
	*
	Should output something like

	```
	 openjdk version "11.0.6" 2020-01-14
	 OpenJDK Runtime Environment (build 11.0.6+10-post-Raspbian-1deb10u1)
	 OpenJDK Server VM (build 11.0.6+10-post-Raspbian-1deb10u1, mixed mode)
	```
	 
	 


#### 1.1 **Maven** (exhaustive procedure can be found [here](https://xianic.net/2015/02/21/installing-maven-on-the-raspberry-pi/))

This example project as Maven as dependencies manager, you need to install it before build the exectubale **.jar**
* Download the Maven binary tar.gz (in this example we'll use v3.6.3) 

	`wget https://downloads.apache.org/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz`

* Extract it ...
		
	`sudo tar -xzvf apache-maven-3.6.3-bin.tar.gz`

* ... and move it onto the **/opt** folder
		
	`sudo mv apache-maven-3.6.3 /opt` 
	
* Tell your shell where to find maven (available for all users)

	`sudoedit /etc/profile.d/maven.sh`
	
	* Enter 
	
	```
	export M2_HOME=/opt/apache-maven-3.6.3
	export "PATH=$PATH:$M2_HOME/bin"
	```

* Check the correct installation of maven using 

	`mvn -version`
	
	* Should output multiple line, starting with `Apache Maven 3.6.3  ...`

Alright, Java and Maven are installed ! Let's jump to next part.

***

### 2. **NodeJS**
You'll often need to access and play with the backend (*i.e* Java) application through  user interface (UI). We'll setup a common tool called **Angular**, based on a famous *JavaScript* library **Node.js**.

> **About Node** : [...] server-side execution of JavaScript code. (Doc [here](https://nodejs.org/en/))
 
> **About Angular**. [...] an app-design framework and development platform for creating efficient and sophisticated single-page apps. (Doc [here](https://angular.io/start)) 

* Download and install `node` & `npm` libraries 

	`sudo curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -`
	
	`sudo apt install nodejs`

* Check installations 
	
	`node --version && npm --version`
	
	* Should output something like
	 
	```
	v12.14.1
	6.13.4
	```
***

### 3. **Docker**
In this part we'll install and configure Docker as the last part of a continuous deployment (*CD*) process.  Doc [here](https://phoenixnap.com/kb/docker-on-raspberry-pi).

* Get the installation script from the docker website
	
	`curl -fsSL https://get.docker.com -o get-docker.sh`

* Execute the script (could take few minutes)

	`sudo sh get-docker.sh` 	

* Add a user to the group "Docker" (replace "pi" with your username)
	
	`sudo usermod -aG docker pi`

> There is no specific output if the process is successful. For the changes to take place, you need to log out and then back in. Doc [here](https://phoenixnap.com/kb/docker-on-raspberry-pi).

* Log out and in, and check the Docker version and correct installation 
	
	`docker version`
		
	* Should output something like
	
	```
	Client: Docker Engine - Community
	Version:           19.03.8
	```

### 3.1 **Docker Compose**

A multi-container tool, which is very helpful to create, start and manage all the services needed in a project. You can learn more about each case in [Common Use Cases](https://docs.docker.com/compose/#common-use-cases) from the official Docker doc.

* [**WARNING**] (doc [here](https://dev.to/rohansawant/installing-docker-and-docker-compose-on-the-raspberry-pi-in-5-simple-steps-3mgl)). In some cases, with ARM designed hardware such as**Raspberry Pi**, the installation of Docker Compose need to be done via the **Python** package manager, aka **pip**. 
* Run the following commands to install Docker Compose *through* python package manager. 

	`sudo apt-get install -y libffi-dev libssl-dev python3 python3-pip`

	`sudo apt-get remove python-configparser`

	`sudo pip3 install docker-compose`


* Check the Docker Compose version and correct installation 
	
	`docker-compose --version`
		
	* Should output something like
	
	```
	docker-compose version 1.25.4, build ...
	```     

***

That's all folks ! you can now clone millions of projects using *Java* with *Maven* for backend and *NodeJS* for frontend, and Dockerize them !
